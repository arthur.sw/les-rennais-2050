# Les Rennais 2050


## Le projet Les Rennais 2050


Projet de magazine collectif intitulé "Les Rennes 2050". L'idée est de reprendre le format des "Rennais", le magazine de la ville de Rennes, mais de l'écrire de manière collective, comme si on était en 2050, dans un monde idéal. L'objectif est de réinventer l'imaginaire collectif, de rêver ensemble d'un idéal ; pour peut-être se donner les moyens de trouver un chemin pour y parvenir.

Une démarche possible consiste à imaginer le monde idéal dans lequel on aimerai vivre. Pour ce faire, on peut réfléchir à plusieurs grandes thématiques (logement, éducation, agriculture, transport) que l'on développe point par point. Evidemment, imaginer un monde idéal mais réaliste est un exercice très difficile, car cela demande une grande connaissance du monde et des mécanismes qui le régissent. L'objectif des Rennais n'est pas de proposer des visions ultra-réalistes qui seraient de toute façon difficile à élaborer ; mais c'est plus de faire rêver en imaginant des possibles futurs souhaitables et agréables. L'imperfection ne doit pas être un frein, mais les participants doivent au contraire se sentir libre de créer et partager ce qu'ils ont envie.

TODO: expliquer l'objectif, l'aspect politique de la démarche, comme il peut être mal perçu / reçu et pourquoi ça a du sens. Combatre le fatalisme, qui va avec l'individualisme, et surtout la prophétie auto-réalisatrice. Parler de l'engagement collectif (et la plateforme d'engagement, de la théorie des jeux ?).
En se mettant dans un état d'esprit ou tout est réalisable, il est possible d'imaginer des actions politiques qui semblent impensables dans le présent. Ainsi, on peut créer des prophéties auto-réalisatrices positives. Par exemple, il est actuellement presque impensable de changer les comportements très rapidements afin de faire face au réchauffement climatique. Pourtant, en imaginant que cela est possible, on peut se permettre de réfléchir aux comportements à modifier, aux futurs souhaitables qui en découleraient, et aux manières de les faire advenir.


## Scénarios

### IA vs Hackeurs

Un milliardaire russe dévevloppe une IA dans un but imperialiste.
Au début ça fonctionne mais après l'IA évolue et devient humaniste.
L'histoire parle de hackeurs de l'ombre qui manigancent qqch, on ne sait pas quoi.
L'IA élabore un ensemble d'étapes pour mettre en place la paix dans le monde, et des sociétés apaisées et heureuses.
Des rumeurs disent que les hackeurs ont pris le contrôle de l'IA et que c'est pour ça qu'elle est devenue humaniste. 
A la fin il y a un retournement de situation: le milliardaire explique que son plan était de paraitre malveillant pour éviter d'être censuré. En effet, une IA surpuissante développée sans contrôle autoritaire aurait été soumise à des régulations qui aurait ralenti son développement, et des guerres auraient éclatées empêchant la réalisation de tous plans vertueux. A la fin, on ne sait plus si les hackeurs se sont fait passé pour une IA pour avoir la légitimité d'instaurer la politiques qu'ils souhaitaient.


Elon Musk lance une IA, il dit que c'est ouvert mais on sait que c'est pour gagner de l'argent.
Les Druides Ubrains, collectif de hackers Rennais, trouvent ça ridicules et décident de s'infiltrer dans le système, pour voir.
Une milliardaire russe lance des cyber-attaques contre des banques partout dans le monde.
Les geeks des cavernes, nerds obscures organisés en réseau mondiale, comprennent que les attaques ne plaisantes pas.
Elon Musk affirme que son IA arriver à stopper les attaques, et embauche massivement pour l'améliorer et sauver les banques.
Jacques, brillant doctorant en mathématiques, se laisse convaincre. Il participe au développement de l'IA qui devient de plus en plus puissante.
Une IA tchat est dévelopée, tout le monde peux discuter avec elle.
Les druides urbains injectent en secret des textes militants dans l'ensemble d'apprentissage de l'IA.
Les cyber-attaques continues mais beaucoup sont mitigées par l'IA, ce qui donne bcp de crédibilité à Elon Musk et à son IA.
Le monde se tranquilise. Les druides ubrains, les geeks des cavernes et Jacques font tous à leur manière de la politique.
Curieusement, ils transforment le monde. En fait ils sont influencé par le tchat de l'IA sans le savoir.
Tout est transformé : la politique, l'industrie, l'écologie, le sociale, le transport, l'éducation, la santé, etc. 
Enfin, le monde semble en paix, tout le monde est heureux, et les cyber-attaques cessent.
Gros retourenement de situation : la milliardaire russe fait une annonce public pour dire qu'elle est à l'origine des attaques et que ça lui a permis de financer le tchat qui a en fait manipulé tout le monde.
2nd gros retournement de situation : Elon Musk explique que c'est son IA qui est à l'origine du plan de la milliardaire russe. Elle était en fait sous son influence sans le savoir.
3ème gros retournement de situation : en fait, les geeks des cavernes laissent entendre que le tchat n'est pas du tout une IA, mais qu'ils écrivaient tout manuellement.


Elon Musk lance une IA, il dit que c'est ouvert mais personne n'est dupe et comprend que c'est pour gagner de l'argent.
Au début ça fonctionne mais après l'IA évolue et devient humaniste.
L'IA élabore un ensemble d'étapes pour mettre en place la paix dans le monde, et ça fonctionne.
A la fin, on ne sait plus si les hackeurs se sont fait passé pour une IA pour avoir la légitimité d'instaurer la politiques qu'ils souhaitaient.


### Espero

En créant une fresque utopique collective, un petit groupe d'amis a réussi à inspirer de plus en plus de personnes à se joindre à leur projet, ce qui a conduit à une véritable intelligence collective et à une action commune. La fresque a permis aux participants de rêver ensemble d'un monde meilleur et de partager leur vision, tout en construisant un avenir souhaitable. En fin de compte, cette initiative a permis de réunir des personnes de toutes les couches de la société, de tous les âges et de toutes les professions pour unir leurs efforts en vue de créer un monde plus équitable, plus résilient et plus épanouissant.

### Les Rennais 2050

Voir ci-dessus.

### Oeuvre d'art pétition pour que toute l'humanité s'engage à ne plus utiliser d'armes


### Pillules et striatum

Scénario science fiction : une pillule permet, après prise tous les jours pendant deux semaines, de débrancher le striatum, et donc avoir un comportement bcp plus adapté à la vie en communauté. L'humanité se sépare en deux camps : ceux qui prennent la pillule pour l'intérêt général, ceux qui ne la prenne pas pour rester humain.

### L'IA remplace les professions intelectuelles, mais pas les physique

### Simulation de société

### Grande négociation entre les ultrariches et les autres. 

Puis réflexion sur les inégalités restantes.
Est-il possible de concevoir un scénario pour garantir le maintien du bonheur des ultrariches en prenant leur fortune? Quel est le scénario de transition qu'ils seraient prêt à accepter?

## Société idéale

Une société utopique idéale est une société dans laquelle tous les individus vivent en harmonie, en expérimentant l'équilibre parfait entre la liberté individuelle et la responsabilité collective. Cette société se caractériserait par plusieurs éléments clés :

  - Égalité sociale : Dans une société utopique, tous les individus seraient traités avec le même respect et la même dignité, indépendamment de leur race, de leur sexe, de leur religion, de leur âge ou de leur statut social. Il n'y aurait pas de discrimination, de préjugés ou d'oppression, ce qui garantirait à chacun des chances égales de s'épanouir.
  - Prospérité et stabilité économiques : En utilisant efficacement les ressources et en adoptant des pratiques durables, une société utopique maintiendrait une économie stable et florissante qui profiterait à tous. Les richesses et les ressources sont réparties équitablement, de sorte que chacun dispose de suffisamment d'argent pour satisfaire ses besoins et mener une vie confortable.
  - Éducation et connaissances universelles : Chacun aurait accès à une éducation de qualité, favorisant l'apprentissage tout au long de la vie et l'esprit critique. Cela stimulerait l'innovation et la quête de connaissances, et permettrait aux individus de prendre des décisions éclairées dans leur vie personnelle et professionnelle.
  - Écologie : La société utopique reconnaîtrait l'importance de préserver l'environnement naturel et adopterait des pratiques de vie durables. Cela comprendrait une consommation responsable, la gestion des déchets et des efforts de conservation afin de protéger la planète pour les générations futures.
  - Santé et bien-être : L'accès universel aux soins de santé permettrait à tous les individus de conserver une santé physique et mentale optimale. Grâce à des mesures préventives et à un traitement rapide, la société réduirait les maladies et les souffrances.
  - Paix et sécurité : Les conflits entre individus ou groupes seraient résolus par la diplomatie et la compréhension, ce qui favoriserait un environnement pacifique et harmonieux. Les gens se sentiraient en sécurité et protégés dans leurs communautés, grâce à des systèmes juridiques justes et équitables.
  - Expression culturelle et artistique : La diversité des pratiques culturelles et des expressions artistiques serait célébrée et encouragée, enrichissant la vie de tous les membres de la société. Cela encouragerait la créativité, l'expression personnelle et une compréhension profonde des expériences et des perspectives de chacun.
  - Un sens aigu de la communauté : Dans une société utopique, les gens prennent soin les uns des autres et soutiennent leurs voisins en cas de besoin. Des liens solides se créent entre les individus, la confiance mutuelle et la coopération étant les fondements d'une communauté cohésive et prospère.
  - Croissance et épanouissement personnels : Les besoins fondamentaux étant satisfaits et les possibilités d'amélioration personnelle étant nombreuses, les individus auraient la liberté et la motivation de poursuivre leurs passions, de réaliser pleinement leur potentiel et d'apporter une contribution positive à la société.
  - Gouvernance transparente et participative : Un système de gouvernance équitable, responsable et accessible permettrait à tous les citoyens de participer aux processus de prise de décision, en veillant à ce que leur voix soit entendue et leurs besoins satisfaits. 

En résumé, une société utopique idéale donnerait la priorité au bien-être, au bonheur et à la croissance holistique de chaque individu, tout en encourageant un sens aigu de la communauté et du partage des responsabilités. Elle serait définie par l'égalité, l'harmonie, la prospérité et la durabilité, et offrirait à tous ses membres un environnement idéal pour s'épanouir.

## Thèmes

### Sommaire

- Logement -
- Education -
- Travail
- Agriculture
- Abolition des frontières
- Transport +
- Nomadisme
- Culture et vie politique 
- Plateforme de gouvernance +
- Plateforme d'engagement
- Amazone verte / Plateforme de consom-action
- Simulateur de société utopiques
- Economie & Finances / financement participatif
- Culture libre & industrie +
- Valeurs et Philosophie
- Urbanisme, nature en ville
- Loisirs sobres
- Décentralisation et centralisation
- Minorités, féminisme, inclusion
- Réseau smile & outils partagés
- Publicité
- Divers
- Autres thèmes

### Logement

#### Rénovation automatique des logements

un scanner 3D permet de faire des plans automatiquement, des panneaux d'isolation sont conçu sur mesure, grâce à la fabrication numérique, personnalisable avec des motifs 3D choisis par les habitant ; les panneaux sont ensuite livrés et il n'y a plus qu'à les coller avec la colle fournie (à mélanger avec de l'eau dans le mélangeur prévu à cet effet). Tout se fait de manière autonome, les plans, les outils, les machines et les matériaux sont livrés et il n'y a plus qu'à suivre les instructions.

#### Politiques du logement

- Une grande politique de rénovation et d'isolation des bâtiments est mise en place, fournissant de nombreux emplois durables, physiques et très utiles et permettant d'économiser une quantité phénoménale d'énergie.
- En même temps, la construction de nouveaux bâtiments est beaucoup plus sobre en énergie et basée sur les matériaux locaux comme le bois, la terre et la paille, ce qui demande plus de main d'oeuvre que les constructions en bétons, mais offre des conditions de travail beaucoup plus agréables.
- Le secteur du logement devient donc une source abondante d'emplois.
- Le travail est repensé et il est courant de faire un mi-temps dans une activité intelectuelle et un autre dans une activité physique (un travail dans la construction en terre-paille par exemple).
- A l'échelle des villes, les bâtiments sont repensés pour avoir plusieurs fonctions. La distinction entre bureaux et logement n'est plus aussi nette. Logements et bureaux sont utilisés autant pour dormir que pour travailler. Le coworking et le télétravail se sont généralisé, et l'organisation des emplois du temps a été réorganisé de façon à ce que le taux d'occupation du bâti est proche de 100% autant le jour que la nuit, tout au long de l'année.
- Une plotique de redistribution des logements est mise en place. De plus, les logements vacants sont ré-employés. Combiné à l'optimisation du taux d'occupation des bâtiment, cela premet de loger tout le monde et de largement réduire les inégalités sociales. La partie du salaire consacré au logement devient dérisoire.
- La propriété privée n'est plus aussi sacrée qu'au début du sciècle.
- L'échange de logement est devenu très commun. Une plateforme permet de plannifier la location de différents logement tout au long de l'année pendant que le notre sera occupé par d'autres personnes. Les échanges de logement permettent de partir en vacances, et certains adoptent même un mode de vie nomade. Les changements sont possibles pour deux raisons. Les échanges se font tout naturellement et tout est organisé pour limiter les risques d'abus et d'incidents.
- On a plus autant d'attache à un lieu en particulier et on aime changer de style.
- L'échange de maison généralisés, systématiques, institutionnalisés, garanties
- Une politique d'organisation fédérale permet de mieux équilibrer l'usage des batiments dans chaque région.
- Tous les bâtiments sont entièrement végétalisés: toitures, terrasses, façades, entrées, sols, interieurs, etc.
- Tous les plans des bâtiments sont libres, disponibles, détaillés, réutilisables, modifiables.

### Travail

- Les travailleurs ont conscience qu'ils travaillent pour le bien commun, pour eux même et pour les autres.
- Les entreprises ne se font plus concurrence mais sont les membres d'une même grande famille coopérative qui œuvre pour le bien de l'humanité.
- Les emplois doivent obligatoirement être diverisifés, les tâches trop répétitives et pénibles ne pouvant être qu'occasionnelles.
- Le travail est inclusif et permet aux personnes en situation de handicape ou en difficulté et aux personnes moins favoriser de s'intégrer, de participer à l'effort collectif et d'obtenir un salaire. Cette inclusion marquée repose sur une grande tolérance envers les parcours de vie de chacun et chacune, réduisant considérablement le stresse au travail.
- Il n'y a plus vraiment de séparation entre métiers de service et métiers physique, les emplois mélangent différentes activités à la fois physiques et intellectuelles.
- La méritocracie n'est plus utile ni souhaitable. Les emplois sont conçu pour être le moins penible et le plus agréable possible. La valorisation au mérite ne prend pas en compte les spécificités et parcours de vie de chacun.e.
- L'élitisme et la sur-spécialisation sont abandonnés, laissant place aux compétences transdisciplinaires, aux parcours de vies atypiques, et aux profils hybrides qui intègrent différentes compétences et expériences.
- Il est très facile de changer d'emploi, cela est même encouragé. Changer d'activité permet de s'épanouir et de s'enrichir personnellement, mais aussi de monter en compétance dans des domaines très divers et ainsi avoir une vision globale des choses.
- Un revenu de base permet de gommer les différences entre travail rémunéré et non rémunéré. Confier ses enfants à une garderie ne génère pas plus d'activité (PIB) que de les garder à la maison. 
- Le temps de travail hebdomadaire n'est plus réglementé. Le revenu de base permet de vivre sobrement mais convenablement, et il est possible de travailler autant que l'on souhaite pour completer ses revenus. Un mécanisme politique permet de maitriser et limiter l'écart entre les plus riches et les plus pauvre.
- Un écart maximal entre les plus hauts et les plus bas revenus est fixé (3x?) de façon à ce que l'écart entre les plus riches et les plus pauvres ne puissent pas s'accroitre indéfiniment.
- Le travail est largement ré-organisé en sept secteurs clés essentiels : l'agriculture, le BTP, l'éducation, l'industrie, le social, la culture et la santé. Le transport est automatisé. Il n'y a plus de bullshit jobs, tous les emplois sont parfaitement utiles. Les activités les plus valorisées sont celles qui sont les plus utiles au bonheur collectif.

### Abolition des frontières

### Plateforme wikipedia pour l'éducation

- La plateforme est collaborative, très ouverte, simple, gratuite, accessible, comme Wikipedia. 
- Tous les professeurs du monde y partagent leurs cours, sur tous les sujets. 
- Il y a des cours théoriques, des travaux dirigés, des travaux pratiques, des vidéos, des exercices interactifs.
- Les étudiants, c'est-à-dire tout ceux qui le souhaitent, peuvent définir le cursus qu'ils veulent, à la carte. 
- Ils sont encouragé à s'entraider : une fois qu'ils ont validé un cours, ils sont incités à l'enseigner aux autres. 
- La plateforme permet d'apprendre en continue pendant toute sa vie ; à temps plein, en alternance, ou par plaisir.
- Des examens adaptés sont disponibles, et permettent d'obtenir des diplômes reconnus internationalement.

### Éducation

- Tout le monde à la possibilité de créer son cursur personnalisé, d'explorer toutes les disciplines, de se spécialiser ou d'avoir un parcours pluridisciplinaire avec des compétences dans des domaines extrèmement variés.
- Tous les cours sont disponibles en ligne sur le wikipédia de l'éducation, MOOC vidéos, leçons, exercices, cm, TD, tout
- Il n'y a plus de distinction franche entre la formation et le travail, on se forme en travaillant et il est facile de passer d'une phase d'apprentissage à un phase de travail. Les emplois sont repensés pour intégrer des formations adaptés.
- L'entraide est au coeur de l'apprentissage : le système encourage les apprennants à partager leurs connaissances aux autres. Une fois une connaissance ou une compétence acquise, il est valorisé de la transmettre à ceux qui le souhaitent. 
- L'éducation n'a plus pour objectif principale de former les citoyens à une activité unique, mais son objectif principal est de fournir les connaissances et compétances nécessaires pour bien vivre en société. Cela signifie qu'une grande partie de l'enseignement concerne la politique au sens large (sciences politiques, communication, intelligence collective), les connaissances fondamentales dans toutes les sciences (sciences humaines et sociales, sciences formelles et sciences de la nature), mais aussi la connaissance de soi, la valorisation des talents et potentiels, l'amélioration de la qualité de vie, et la réalisation de ses aspirations et de ses rêves.
- L'epistémologie est enseignée très tôt ; on apprendre comment construire la connaissance. Ainsi, la science est perçue comme une méthodologie fiable de création de la connaissance, ses limites sont également mises en avant.
- Un ensemble de compétences fondamentales est fortement conseillé et valorisé. La qualité fondamentale de chaque compétence ou connaissance est évaluée. De cette façon, on repère facilement les connaissances nécessaires à un grand nombre de compétences et celles très spécifiques utilent à un domaine de compétences restreint.
- Le système permet de bien comprendre comment chacun·e peut se rendre utile à la collectivité, quels sont les activités nécessaires à la société, comment vont évoluer les différents secteurs d'activités dans les décénies à venir, et en quoi les emplois disponibles sont conçus pour être intéressants et peuvent nous épanouir.
- Les diplômes et unités d'enseignement sont internationnales et reconnues partout dans le monde.
- Le nombre d'élèves par classe est réduit (10 max) de façon à ce que les professeurs puissent prendre soin de toutes et tous.
- Les professeurs ont une formation principalement axée sur la pédagogie. Ils étudient les principes fondamentaux des sciences de l'éducation et apprennent à mettre en place différents pédagogies adaptés à différents élèves et contexts.
- L'éducation joue un vertiable rôle d'accompagnement des élèves, de façon à ce qu'ils puissent mener une vie épanouissante, et qu'ils trouvent une place confortable dans la société.
- Un des compétences fondamentale enseignée dès le départ est la capacité à apprendre ; on enseigne très tôt comment apprendre à apprendre.
- pourquoi un parcours personalisé
- Puisqu'un objectif majeur de l'éducation est l'épanouissement de l'individu, certaines réflexions philosophique comme la question du bonheur sont abordés.
- Les notions essentielles à la vie politique sont enseignées (diversité des opinions, tolérance, empathie, théorie des jeux, etc.). 

### Transport

- Les modes de transport doux (marche, vélo, trotinette, gyropode, skateboard, etc.) sont généralisés en ville. Il n'y a plus de voitures individuelles.
- Les véhicules motorisés restant (mini-bus, navettes pour personnes à mobilité réduite, caisses de transports) sont électriques, autonomes, ultra-léger, efficaces, silencieux et 100% sûrs.
- En conséquence, la qualité de l'air est excellente, les villes sont parfaitement calmes et elles deviennent entièrement végétalisées. Il est agréable de se promener dans la rue sans craindre de croiser une voiture potentiellement dangeureuse.
- Le transport de marchandise se fait en partie par des vélos de transport équipés de grandes remorques, et en partie avec des caissons autonomes qui optimise leur itinéraire pour géner au moins la circulation (livraisons de nuit).
- Le transport de personnes à mobilité réduite se fait beaucoup en vélo taxi.
- La système de vélo en libre service est extrement efficace.
- Les métros et trams ont des rames pour ranger vélos, trotinettes et autres moyens de transport léger.
- Le réseaux ferré est extremement développé. Les trains relient toutes les villes et villages ; ils sont confortables, spacieux et fréquents.
- Il existe encore quelques voitures et camions électriques et autonomes pour desservir les coins les plus reculés.
- Les passagers peuvent discuter, jouer aux cartes ou siroter un cocktail dans le salon-habitacle des transporteurs.
- Le transport de marchandise sur de longues distances se fait principalement sur des cargos à voile et à cerf-volant, mais aussi sur des voiliers et des vieux gréments. Les péniches jouent également un rôle important.
- Les touristes ne prennent plus l’avion tant ils préfèrent le plaisir d’une croisière sur un voilier chargé de café et de cacao, de tissus ou d’épices.
- Le vélo, le catamarrant et le kayak itinérant sont les tran-sports les plus courants.
- Il existe quelques avions électriques, mais ils sont très peu utilisés (on ne comprend plus l'intérêt de se déplacer très vite et très loin sans profiter du voyage et des richesses qu'il représente).

### Culture politique

Des notions fondamentales nécessaire à la vie durable en société / politique sont enseignées : biais cognitifs, jeu de la vie, prophétie auto-réalisatrice, phénomène de popularité, etc.

Des festivals de création et de politique

Fin du mythe de la technologie salvatrice. 

#### Vie Politique

- La politique est omniprésente dans la vie des citoyens ; ceux-ce consacrent 5 à 10 heures par semaine à l'organisation de la vie collective.
- L'éducation joue un rôle majeur dans l'enseignement des fondamentaux politiques. On enseigne autant l'histoire et les grandes idées politiques que les différents partis en expliquant l'origine et la diversité des points de vue. 
- L'organisation politique repose sur plusieurs institutions, à commencer par les agoras et les conseils citoyens.
- Les réunions politiques sont fréquentes.
- La vie politique intègre également le concept de démocratie liquide, également appelée démocratie délégative. Cela signifie que les citoyens ont la possibilité de voter directement sur les décisions ou de transférer leur droit de vote à un délégué de leur choix. Ce principe étant valable également pour chaque délégué, la délégation est dite « en cascade ». Ce système de délégation permet aux citoyens de s'engager de manière flexible dans les processus de prise de décision en fonction de leurs intérêts et de leur disponibilité. 
- Le pouvoir est décentralisé, l'organisation est plus fédérale, chaque région a son autonomie mais a des obligations de redistribution et collaboration avec les régions voisines.
- La structure de l'organisation politique est très lisible, et il est facile de s'impliquer à l'échelle locale autant qu'à l'échelle globale.
- Les citoyens sont conscients des biais cognitifs et autres idées séduisantes mais trompeuses qu'ils doivent éviter pour maintenir un système social bien organisé.
- La vie politique physique et numérique sont intriqués.
- De très nombreuses citoyennes et citoyens prennent grand plaisir à débatre de questions philosophiques et ethiques.
- La théorie des jeux est connue de toutes et tous, et permet de mieux comprendre les mécanismes politiques.  
- Les convension citoyennes (type convension citoyenne pour le climat) sont très fréquentes. Elles rassemblent des experts sur un sujet et des citoyens chargés de réfléchir aux questions politiques soulevé par ce sujet.

### Plateforme de gouvernance

### Plateforme d'engagement

- Enagement équitable (jets privés)
- Réputation sociale

### Amazone verte / Plateforme de consomaction

- Pour faire face au réchauffement climatique, une taxe carbone juste et équitable est nécessaire. Nécessaire pour que l'emprunte carbone de la consommation soit visible et prise en compte ; juste et équitable parce que les plus pauvres ne peuvent pas supporter la même pression économique que les plus riches.
- Une telle taxe était trop difficile à mettre en place avant 2025 parce que les implications géopolitiques étaient trop importantes. 
- Pourtant, de nombreux citoyens comprenaient les enjeux du réchauffement climatique et souhaitaient agir pour lutter contre les émissions de gaz à effets de serre. Eux souhaitaient que la taxe soient imposée, mais le passage à action était bloqué à l'échelle des états nations.   
- La plateforme Jungle permet de vendre des produits et services au prix auxquels ils couteraient si la taxe carbone avait été instauré. Les consommateurs qui le souhaitent peuvent donc acheter des produits en ayant la garantie que l'impact carbone soit répercuté dans le prix.
- Les bénéfices de la plateforme sont conséquents ; la taxe carbone n'ayant pas été instaurée, les produits et services sont vendus plus chers que ce qu'ils coutent réelement (même si ce cout ne prend pas en compte le cout des conséquences catasrtophiques qui adviendront à l'avenir). Les bénéfices sont employés pour le développement de projets vertueux, écologiques et sociales.
- La plateforme propose d'abord des biens et services "éthiques", c'est-à-dire avec un impact réduit sur l'environnement et un impact social positif. Lorsqu'il n'existe aucune solution éthique, la plateforme propose les alternatives conventionelles les moins délétère pour l'environnement et les être humains, mais surtaxés de manière proportionelle à leurs effets négatifs.
- La plateforme permet aux utilisateur de suivre précisemment leur consommation et leur emprunte carbone, elle donne des conseils aux consommateurs pour qu'ils minimisent leur impact écologique et social.
- Les utilisateurs ont un quota carbone annuel à ne pas dépasser ; ils ne peuvent pas consommer au dela d'un certain seuil d'émission de gaz à effet de serre.
- La plateforme permet aux consommateurs de faire retours utilisateurs constructifs et pris en compte par les concepteurs de produits et services. Au lieu d'un simple espace commentaire, la plateforme propose un ensemble d'outil permettant aux utilisateurs de s'impliquer dans la conceptions des biens et services. Une veritable collaboration entre utilisateurs et concepteurs de produits se met en place.
- Pour chaque produit et service, la plateforme fournie une mine d'information (l'histoire du produit, le lieu de production, la provenance de chaque sous-produit, les couts des matières première, de production, de transport et de vente, l'impact social, etc.). Elle est liée à la plateforme de financement de projet.
- La plateforme est dirigée de manière participative grace à la plateforme de gouvernance ouverte.

### Autres plateformes numériques

- plateformes citoyennes : budget participatif, réseau cyclable
- todo-liste de l’humanité

### Simulateur de société utopiques

### Plateforme de financement

- L'outil permet de financer des projets en mettant en relation les différentes parties concernées : 
  - les entrepreneurs, 
  - les spécialistes du domaine, chargés d'estimer la faisabilité du projet,
  - les conseillers, experts en développement et en financement de projet,
  - les investisseurs, gros financeurs et particuliers (crowdsourcing),
  - les travailleurs impliqués dans le projet,
  - les utilisateurs, très impliqués dans le projet.
- La plateforme permet bien comprendre tous les aspets du projet : la génèse, les objectifs, les business plans, les différents scénarios de réalisation envisagés, le budget nécessaire aux différents plans, les risques, les oportunités, le retour sur investissement, etc.
- La conception et le développement du projet se font de manière coopérative. Les utilisateurs finaux peuvent ainsi donner leurs avis et contribuer à son évolution. La culture des utilisateurs les incite à s'impliquer fortement durant tout la vie du projet, de façon à ce que le résultat correponde précisément à leurs attentes.
- Le plan de financement du projet est public et détaillé. Tous les couts et recettes sont lisibles et il n'y a pas de secrets d'affaires.
- Deux versions de la plateformes sont étudiées par le simulateur de société : 
  - Dans la première version, chacun peut investir dans le projet comme il le souhaite avec ses fonds propres. Dans cette version, un mécanisme permet de limiter l'accumulation de richesse pour éviter que l'écart entre les plus riches et les plus pauvres ne s'accroit indéfiniment.
  - Dans la seconde version, l'état finance la totalité du projet par l'intermédiaire des conseillers, mais les avis de toutes les parties concernées sont pris en compte et influence le déroulement du projet.  
- Le prix peuvent-être fixes ou libres. Un prix libre signifie que chacun peut choisir combien il paie, en fonction de ses envies et de ses moyens. Des valeurs minimales et maximales du prix libre peuvent être définis. La plateforme affiche combien le produit ou service doit se vendre en moyenne, et combien il se vend effectivement. De plus, des projections permettent de voir comment va évoluer le projet si le prix de vente moyen diminue ou augmente. Le consommateur peut donc adapter finement son prix d'achat.
- La plateforme de gouvernance ouverte permet à tous les acteurs concernés par le projet (utilisateurs compris) de définir le développement et l'évolution du projet. La gestion des conflits (conflits d'intérêt et autres) est gérée sur la plateforme de gouvernance ouverte. 

### Revenu de base

### Ecologie

### Culture et industrie libre

- La fabrication et la réparation sont inspirées de l'Atelier paysant. L’Atelier Paysan est une coopérative qui accompagne les agriculteurs et agricultrices dans la conception et la fabrication de machines et de bâtiments adaptés à une agroécologie paysanne. En remobilisant les producteurs et productrices sur les choix techniques autour de l’outil de travail des fermes, ils retrouvent collectivement une souveraineté technique, une autonomie par la réappropriation des savoirs et des savoir-faire.
- Toutes les machines et tous les objets étaient conçu selon les préconisations du think tank mondiale negawatt : montables, démontables, réparables, entièrement recyclables, voir biodégradables, durables, quasi incassables, modifiables, améliorables, personnalisables, admirables, bref, incroyables.
- Tout est libre, c'est-à-dire que les sources et plans de fabrication sont disponibles (en format numérique standard), et que chacun peut les faire évoluer et les adapter comme il l'entend.
- Wikifab est la plateforme qui permet d'héberger toutes les sources et plans. L'outil permet aussi de gérer les forks et est associé à la plateforme de gouvernance ouverte pour que tout le monde puisse participer à l'évolution des différentes version d'un même projet. 
- Il est fréquent qu'un projet soit récupéré et largement transformé pour un usage différent de celui d'origine.
- Les monopoles sont très réglementés, il sont uniquement possibles lorsque les alternatives ne sont pas pertinentes (réseau ferré, réseau électrique, etc.). 
- De la même manière, la concurence est strictement contrôlée : le nombre de projets similaires développés en parallèle à l'échelle mondiale est limité, et une colaboration minimale entre concurents est obligatoire (les plans de fabrication étant de toute façon disponibles, et le secret des affaires étant inexistant, la collaboration se fait naturellement). De cette façon, il y a des échanges entre les différentes branches évolutive d'un projet. Tous les acteurs et utilisateurs d'un projet ont conscience d'oeuvrer pour le collectif.
- Les usines sont ouvertes à tous, et tout le monde peut y travailler en s'inscrivant sur les calendriers partagés. Cela permet de découvrir un vaste ensemble de métiers et d'apprendre toutes sortes de compétences. Tout était accessible, les experts enseignaient aux novices, et cela créait des vocations.
- En plus des usines, les laboratoires de fabrication et réparation permettent d'étudier, de développer et de redonner vie à toutes sortes de machines ou d'objets.
- Ces laboratoires ont remplacés les boutiques des centres villes, et les centres commerciaux des périphéries et des campagnes. Les lieux de consommation sont devenus des lieux d'action (fabrication et réparation).
- Comme les plans sont tous disponibles sur l'encyclopédie de fabrication wikifab, chacun peux réparer sa machine à laver ou sa tondeuse à gazon en ré-imprimant une pièce en 3D (très pratique pour refaire une roue de lave vaisselle), ou en la découpant dans le fablab du coin.
- Tout le monde est autant créateur qu'utilisateur et s'approprie la technique.
- Les pièces sont standardisées, utilisées sur plusieurs machines différentes, faciles à trouver dans le fameux catalogue de bricolage mondiale, et aisaiment récupérables dans les grands ateliers de démontage.
- Le même composant moteur est adapté et utilisé pour la perceuse, la visseuse, la machine à coudre et le mixeur plonguer ; le même écran et les mêmes boutons sont utilisés pour contôler le four, la machine à laver, le micro-onde et le lave vaisselle. 
- La fabrication, l'industrie, la réparation, le recyclage, tout est le plus local possible.
- Cela va de soi, le nombre et la qualité des matériaux utilisé sont maitrisés afin de faciliter le recyclage. - Les plastiques sont toujours très utilisés, mais seulement sous des formes recyclables à l'infini ou biodégradable. Et chacun sait comment le trier, le travailler, l'usiner, et faire en sorte qu'il ne sorte pas du cycle de production / deproduction.
- Les systèmes sont interopérables et standard. Il est très facile de combiner les composants de différentes machines pour en créer d'autes.


### Agriculture

- L'agriculture se fait dans de nombreuses petites exploitations, rétablissant une biodiversitée riches et des écosystèmes complexes complémentaires, avec des haies, zones humides, prairies, forêts, etc. Chacun est conscient que ces écosystèmes sont interconnectés et interdépendants.
- Les campagnes sont redynamisées puisqu'il y a besoin de plus de main d'oeuvre, de plus d'agriculteurs. Les villages reprennent vie, la culture s'installe dans les campagnes (concerts, expos, conférences, rencontres, ateliers natures, etc.).
- La nouriture est très majoritairement produite localement, les produits exotiques sont exceptionnels. Chaque ville est entourée des fermes et maraichés qui produisent les fruits, légumes, céréales, viandes et poissons qu'elle consomme.
- La main d'oeuvre nécessaire à l'agriculture permet de largement réduire le chomage, et contitue un ensemble d'activités salariées physiquement et intéllectuellement épanouissante.
- Les engrais synthétiques ne sont plus utiles, car les déchets organiques (déchets verts, déchets alimentaires, dejections animales et humaines) sont entièrement récupérées, leurs gestion est optimisée et ils sont suffisants pour fertilliser les cultures.
- La taille réduite des exploitations et la main d'oeuvre importante permettent d'éviter l'usage des phytosanitaires.
- L'agriculture est vertueuse pour l'environnement et se fait dans le respect des animaux. Toutes les cultures et élevages sont labélisés bio.
- La dépendance énergétique de l'agriculture est réduite. Les machines sont électriques ou fonctionnent au méthane produit ultra-localement.
- Les techniques de productions ne nécessitent l'utilisation massive des machines.
- La traction animale est couramment utilisée.
- Les instectes regagnent du terrain. Après le déclin des popullations d'insects de -80% entre 2000 et 2025 ; une augmentation importante du nombre d'insects permet d'atteindre le niveau des années 20.
- Les oiseaux et toute la faune sauvage bénéficient également de cette accroissement des populations d'insects. Les chants d'oiseaux sont plus nombreux et plus sonores, les printemps plus joyeux.
- La gestion de l'eau est optimisées de manière démoctratique par tous les agriculteurs. Des mini-bassines sont érigées pour faire face au manque d'eau. Un système naturelle de purification et de refroidissement de l'eau par les plantes permet de préserver la qualité de l'eau des bassines et de réduire les pertes par évaporation.
- Les algues vertes disparraissent des côtes Bretonnes et des autres régions, puisque tous les nitrates sont valorisés dans les champs.
- Partout, les rivièvres reprennent une belle couleur, l'eau est pure et on peut s'y baigner à toute saison.
- Des chemins de randonnées et des voies cyclables sillonnes les campagnes. Les chemins sentiers suivent les haies, se faufillent des les forêts, longent les champs, les arbres recouvrent les chemins. Les fleurs prolifèrent et la diversité des plantes donne naissance à des paysages toujours différents.


### Valeurs et Philosophie

Sobriété
Bonheur
Sérénité
Utilitarisme
Durabilité
Collaboration et entraide
Universalisme 
Diversité
Mutualisation
Créativité
Inclusion
Bienveillance 
Tolérance
Rationalité
L'égalité
Liberté
Compassion
Justice
L'éducation

### Urbanisme, nature en ville

### Loisirs sobres 

Toutes les activités que l'on peut faire en ayant comme technologie max l'équivalent du vélo

### Décentralisation et centralisation

### Minorités féminisme inclusion

Révolution romantique

### Réseau smile & outils partagés

### Nomadisme

### Publicité

### Autres thèmes

- Santé
- Prison
- Justice

### Divers

- Table 3.85 Gm