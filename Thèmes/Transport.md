Espero - Transport

La mobilité du nouveau monde était articulée selon trois téchnologies du futur :

    le vélo (et la trotinette),
    le train,
    et la voile.

En ville, vélos, trotinettes et longboard circulent librement dans les rues, tout comme les enfants. L’air est pur, la végétation omniprésente, et on entend à nouveau les oiseaux chanter, parfois accompagnés d’un joli son de sonette de byciclette. Les rues sont libérées, grandes, presque vides, on respire, on s’étire, on peut boire un café en terrasse en écoutant le bruit du vent dans les feuilles, ou juste profiter du parterre de fleurs multicolore que les écolier du quartier ont planté pour les abeilles, les hérissons et les coccinelles. Les transporteurs à vélo livrent colis et repas, les taxi-vélo promènent touristes et femmes d’affaire, et quelques caisses roulantes élécriques autonomes transporte des marchandises la nuit en évitant tout encombremant. Métros et trams ont des rames pour ranger vélos et trotinettes.

Les trains relient toutes les villes ; ils sont confortables, spacieux et fréquents ; alors tout le monde est très content.

Il existe encore quelques voitures et camions pour desservir les coins les plus reculés, mais ils ont la particularité d’être autonome, partagés, 100% sûre, et propres. Les véhicules choisissent judicieusement leurs itinéraires pour optimiser le transport de marchandises et de voyageurs ; ceux-la même qui peuvent discuter, jouer aux cartes ou siroter un cocktail dans le salon-habitacle des transporteurs.

Le transport de marchandise sur de longues distances se fait principalement sur des cargos à voile et à cerf-volant, mais aussi sur des voiliers et des vieux gréments. Les touristes ne prennent plus l’avion tant ils préfèrent le plaisir d’une croisière sur un voilier chargé de café et de cacao, de tissus ou d’épices. Les pénnichent aiment transporter des voyageurs sur leur cargaisons ; tandis que oiseaux et autres animaux aprécient leur lenteur et leur délicatesse.
Le catamarrant et le kayak itinérant sont les tran-sports les plus courants, après le vélo bien sur.

Ah, et il n'y a plus d'avions ; ils sont remplacé par des oiseaux migrateurs contents.