
 - Vie politique omniprésente, 25 heures de travail, 10 heures de politique, 
 - Omniprésente dans l'éducation : 
   - enseignement politique qui commence par "comment construire la connaissance"
   - enseignement détaillé des différents partis, histoires, idées ; les partis participent aux programmes ?
   - diversité des points de vue
 - Agora, réunions régulières, à différentes échelles
 - Vie politique physique et numérique ; intriqués
 - Ethique discuté (végétarisme)


--En 2050, la politique est omniprésente au quotidien.--

Le concept d'afterwork est apparue dans la langue française dans les années 2010, il s'agissait alors de boire un coup avec ses collègues à la sortie du travail. Les temps ont bien changé, la norme est maintenant d'aller faire un tour à l'agora locale ; lieu de rencontre, de discussions intenses, de débat. Dans la culture individualiste du début du second milénaire, il était difficile d'imaginer que la politique puisse être un loisir socialement valorisé, et apprécié du plus grand nombre. A l'époque, il était inconcevable de voir un groupe de personne avec des opinions politiques très différentes avoir une vértiable discussion de fond, amicale et intéressante voir clivante. La société était trop binarisée, les idées politiques trop radicalement opposées pour qu'un terrain d'entente puisse être ne serait-ce qu'imaginé. Ironiquement, ce sont les crises multiples menaçant la survie de l'humanité qui se sont manifestées au début du siècle qui nous ont obligé à confronter nos idées, et à s'impliquer progressivement dans la creation d'un projet de vie collective planétaire.  Lorsqu'il n'y avait plus de travail pour toutes et tous, que l'on avait du mal à se procurer les biens de première nécessité, et que les inégalités étaient toujours croissantes et devenait toujours plus insupportables, les dogmes politiques de l'époque ont perdus leurs crédibilités et il a fallu entièrement repenser notre manière de vivre ensemble dans ce monde.

Un des élément qui a désamorcer la tension grandissante entre les différentes classes sociales est la reconnaissance du fait que chacun était à la fois conditionné et emprisonné dans sa classe. Le bonheur était à l'époque encore largement défini par différence sociale ; c'est à dire que l'on se comparait naturellement à son entourage proche pour évaluer la qualité de sa vie et donc définir si l'on devait s'en satisfaire ou non. Comme on observait systématiquement plus intensément les personnes mieux loties que nous, tout le monde se sentait insuffisamment épanoui, lésé par la société d'une manière ou d'une autre, et l'on rejetait facilement la faute sur les autres. Cette illusion d'optique touchait aussi bien les mieux lotis (qui se comparaient entre eux) que les plus pauvres (qui eux avaient de bonnes raisons de se sentir lésé). Une chaine de frustration était en place depuis des lustres : les plus pauvres enviait la classe moyenne, celle-ci enviait la classe supérieure qui elle même souhaitait devenir encore plus riche. Il y avait toujours quelqu'un de mieux loti et il fallait prouver à notre entourage qu'il était possible de nous élever, sans quoi notre bonheur était impossible. Casser cette chaine de frustration était absolument impossible individuellement. Même en ayant la meilleur volonté du monde, une personne née riche qui donnerait sa fortune dans l'espoir d'une meilleur repartition des richesses n'aurait qu'un impact infime. Ainsi, même si tout le monde était conscient des inombrables problèmes causés par l'accroissement des inégalités ; et parce que tout le monde était également persuadé d'être impuissant face à ce problème, personne n'agissait. Certains se résignaient à adopter une posture individualiste et continuait à tout prix d'accroitre leurs richesses ; convaincu qu'il n'était pas possible de changer l'ordre établi depuis plusieurs générations. D'autres savaient qu'il était possible de stopper cette fuite en avant, mais pour cela il fallait nécessairement agir collectivement, et donc qu'une grande majorité s'engage dans une démarche politique globale. 

La grande avancée culturelle apparut de manière tout à fait inatendue grace à une simple page web du début des années 2000. Ce site, vous le connaissez tous, c'est la plateforme d'engagement collectif ! Un utilisateur s'engage à faire une action si un nombre suffisant d'autres personnes s'engagent à son tour à faire la même chose. Par exemple dans un quartier, un habitant peut s'engager à fleurir la rue et mettre des plantes grimpantes sur sa maison si la moitié des autres habitants s'engagent à le faire. Avant que ce seuil ne soit atteint, personne n'a d'obligation ; mais dès qu'il y a suffisamment de personne à s'être engagées, alors tout le monde se mobilise pour réaliser l'action collective : on voit apparaitre des fleurs et de la végétation dans tout le quartier en quelques semaines ! 







Cette platform d'engagement a longtemps été ignorée du public, et c'est seulement grace aux utopies collectives qu'il a pu se légitimer!



La négociation avec les multi-milliardaires?

