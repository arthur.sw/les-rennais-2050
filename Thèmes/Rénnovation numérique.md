## Prompt

écris le scénario d'une histoire captivante qui raconte comment sera la Rénovation des logements dans un futur proche idéal : un scanner 3D permet de faire des plans automatiquement, des panneaux d'isolation sont conçu sur mesure, grace à la fabrication numérique, personnalisable avec des motifs 3D choisis par les habitant ; les panneaux sont ensuite livrés et il n'y a plus qu'à les coller avec la colle fournie (à mélanger avec de l'eau dans le mélangeur prévu à cet effet).

## GPT3

Dans un futur proche, la rénovation des logements a été révolutionnée grâce à l'avancée technologique. C'est l'histoire de Maxime et Sarah, un jeune couple qui vient d'acheter une vieille maison dans la banlieue parisienne. Ils ont décidé de la rénover pour en faire leur petit nid douillet.

Maxime est ingénieur en informatique et Sarah est designer d'intérieur. Ils sont tous les deux passionnés par les nouvelles technologies et ils ont entendu parler d'une méthode de rénovation innovante qui utilise des scanners 3D pour faire des plans automatiquement. Ils décident d'en savoir plus.

Après quelques recherches, ils découvrent une entreprise spécialisée dans la rénovation de maisons avec des technologies de pointe. Ils prennent rendez-vous pour une consultation et sont surpris par la rapidité et la précision des scanners 3D. En quelques heures seulement, la machine est capable de cartographier l'intérieur et l'extérieur de la maison, et de fournir des plans détaillés de chaque pièce.

Maxime et Sarah sont ensuite invités à choisir les panneaux d'isolation pour leur maison. Les panneaux sont conçus sur mesure grâce à la fabrication numérique, ce qui leur permet d'être personnalisables avec des motifs 3D choisis par les habitants. Sarah s'amuse à choisir des motifs floraux pour la chambre et des motifs géométriques pour le salon.

Quelques semaines plus tard, les panneaux sont livrés directement à la maison. Ils sont légers et faciles à manipuler, et il n'y a plus qu'à les coller avec la colle fournie (à mélanger avec de l'eau dans le mélangeur prévu à cet effet). Maxime et Sarah passent une journée à les poser eux-mêmes, en suivant les instructions simples fournies par l'entreprise.

Au final, leur maison est devenue un véritable bijou. Grâce à la technologie, ils ont pu rénover leur maison en un temps record, avec une précision et une personnalisation inégalées. Ils sont ravis d'avoir fait le choix de la rénovation numérique, et ils sont convaincus que cette méthode deviendra bientôt la norme pour tous les projets de rénovation.

## GPT4

Titre : La Rénovation de Demain

Scénario :

Scène 1 : Sélection du logement

Lieu : Un quartier résidentiel, devant une maison familiale

Nous suivons l'histoire d'Alice et Thomas, un couple qui vient d'acheter une vieille maison dans leur ville natale. Leur objectif est de rénover cette maison en faisant appel à une startup innovante qui promet une rénovation rapide et efficace grâce aux dernières avancées technologiques.

Scène 2 : Rencontre avec l'équipe de rénovation

Lieu : A l'intérieur de la maison

Alice et Thomas rencontrent Julie, une représentante de l'entreprise de rénovation, qui arrive avec un scanner 3D pour effectuer une visite de chaque pièce de la maison. Julie les guide tout au long du processus et explique la façon dont fonctionne chaque étape de la rénovation.

Scène 3: Création des plans de la maison

Lieu : QG de l'entreprise de rénovation

De retour au QG, Julie utilise le scanner 3d pour créer automatiquement les plans de chaque pièce de la maison. Les plans sont ensuite envoyés à l'usine de l'entreprise où ils commencent à produire des panneaux d'isolation sur mesure pour chaque pièce.

Scène 4 : Personnalisation des panneaux d'isolation

Lieu : Atelier de l'entreprise

Pendant que les panneaux d'isolation sont conçus, Alice et Thomas ont la possibilité de choisir parmi une large gamme de motifs 3D pour personnaliser leur espace. En parallèle, ils reçoivent un tutoriel pour l'installation des panneaux d'isolation et d'une démonstration pour mélanger la colle qui sera utilisée pour fixer les panneaux.

Scène 5 : Livraison et installation des panneaux d'isolation

Lieu : La maison en rénovation

Les panneaux d'isolation sont rapidement livrés à la maison d'Alice et Thomas avec la colle spéciale et le matériel nécessaire pour les fixer. Le couple commence alors à coller les panneaux d'isolation sur les murs, suivant les instructions fournies par Julie.

Scène 6 : La transformation

Lieu : La maison en rénovation

Au fur et à mesure qu'Alice et Thomas installent les panneaux, ils sont émerveillés par la transformation de leur maison. Les anciens murs délabrés sont maintenant recouverts de beaux motifs 3D et la température ambiante de la maison devient agréable grâce à une meilleure isolation.

Scène 7 : Le résultat final

Lieu : La maison rénovée

Quelques jours plus tard, la rénovation est terminée. Alice et Thomas sont ravis de leur nouvelle maison, qui allie modernité, isolation et style. Les panneaux d'isolation, conçus sur mesure par une entreprise innovante, ont permis d'obtenir un résultat exceptionnel qui a dépassé leurs attentes. Grâce à la rapidité et la simplicité du processus, ils peuvent désormais profiter pleinement de leur foyer.

---


super! Mais j'aimerai que tout se passe chez Thomas et Alice, et qu'ils soient entièrement autonome: ils reçoivent le scanner avec des instructions et un catalogue de motifs, font le scan 3D, renvoie le scanner avec leurs envies, et reçoivent les panneaux prêt à être colé avec la cole et les outils (qu'ils renverront une fois terminé).


---


Titre : La Rénovation de Demain - Le pouvoir de la technologie

Scénario :

Scène 1 : Sélection du logement

Lieu : Un quartier résidentiel, devant une maison familiale

Nous suivons l'histoire d'Alice et Thomas, un couple qui vient d'acheter une vieille maison dans leur ville natale. Leur objectif est de rénover cette maison en utilisant les dernières avancées technologiques pour gagner du temps et être totalement autonomes durant le processus.

Scène 2 : Réception du scanner 3D

Lieu : A l'intérieur de la maison

Alice et Thomas reçoivent un paquet contenant un scanner 3D avec des instructions détaillées sur son utilisation et un catalogue de motifs pour personnaliser leur espace. Ils commencent alors à scanner chaque pièce pour créer des plans 3D de la maison.

Scène 3 : Personnalisation des panneaux d'isolation

Lieu : A l'intérieur de la maison

Alice et Thomas parcourent le catalogue de motifs 3D et choisissent leurs préférés pour chaque pièce de la maison. Ils envoient ensuite les plans 3D et leurs choix de motifs à l'entreprise, qui commence à confectionner les panneaux d'isolation personnalisés.

Scène 4 : Livraison et installation des panneaux d'isolation

Lieu : La maison en rénovation

Quelques jours plus tard, les panneaux d'isolation sont livrés à la maison d'Alice et Thomas avec la colle spéciale, les outils nécessaires pour la pose et un manuel d'instructions pour les guider. Le couple commence alors à coller les panneaux d'isolation sur les murs, suivant les instructions fournies dans le manuel.

Scène 5 : La transformation

Lieu : La maison en rénovation

Au fur et à mesure qu'Alice et Thomas installent les panneaux, ils sont émerveillés par la transformation de leur maison. Les anciens murs délabrés sont maintenant recouverts de beaux motifs 3D et la température ambiante de la maison devient agréable grâce à une meilleure isolation.

Scène 6 : Le résultat final

Lieu : La maison rénovée

Une fois la rénovation terminée, Alice et Thomas sont ravis et fiers du travail accompli par leur propre initiative. Ils renvoient les outils et le reste de la colle à l'entreprise, prêt à être utilisé par d'autres clients. Leur nouvelle maison allie désormais modernité, confort et style grâce aux panneaux d'isolation personnalisés conçus sur mesure avec l'aide de la technologie.

